
def handler(context, args):
    if 'nombre' in args.keys():
        a = []
        for character in args['nombre']:
            a.append(character)
        return {'result': 'OK', 'datos': a}
    else:
        return {
            'result': 'ERROR',
            'message': 'no hay atributo nombre'
        }
