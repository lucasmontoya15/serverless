FROM python:3.7-alpine3.9

WORKDIR /app

COPY ./requirements.txt /app/requirements.txt

RUN pip install -r /app/requirements.txt

COPY ./scripts/ /app/

ENTRYPOINT [ "python" ]

EXPOSE 5000

CMD [ "worker.py" ]
